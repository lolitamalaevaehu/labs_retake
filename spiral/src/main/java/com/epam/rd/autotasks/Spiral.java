package com.epam.rd.autotasks;

public class Spiral {
    public static int[][] spiral(int rows, int columns) {
        int[][] matrix = new int[rows][columns];
        int num = 1; // число, которое будет заполняться в матрицу
        int topRow = 0;
        int bottomRow = rows - 1;
        int leftColumn = 0;
        int rightColumn = columns - 1;

        while (topRow <= bottomRow && leftColumn <= rightColumn) {
            // Заполнение верхней строки
            for (int i = leftColumn; i <= rightColumn; i++) {
                matrix[topRow][i] = num++;
            }
            topRow++;

            // Заполнение правого столбца
            for (int i = topRow; i <= bottomRow; i++) {
                matrix[i][rightColumn] = num++;
            }
            rightColumn--;

            // Заполнение нижней строки
            if (topRow <= bottomRow) {
                for (int i = rightColumn; i >= leftColumn; i--) {
                    matrix[bottomRow][i] = num++;
                }
                bottomRow--;
            }

            // Заполнение левого столбца
            if (leftColumn <= rightColumn) {
                for (int i = bottomRow; i >= topRow; i--) {
                    matrix[i][leftColumn] = num++;
                }
                leftColumn++;
            }
        }

        return matrix;
    }

    public static void main(String[] args) {
        int rows = 3;
        int columns = 4;
        int[][] result = spiral(rows, columns);

        // Вывод матрицы
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.printf("%3d ", result[i][j]);
            }
            System.out.println();
        }
    }
}
