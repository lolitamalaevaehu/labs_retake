package com.epam.rd.autotasks;

import java.util.Arrays;

class CycleSwap {
    static void cycleSwap(int[] array) {
        if (array == null || array.length == 0) {
            return;
        }

        int lastElement = array[array.length - 1];
        System.arraycopy(array, 0, array, 1, array.length - 1);
        array[0] = lastElement;
    }

    static void cycleSwap(int[] array, int shift) {
        if (array == null || array.length == 0) {
            return;
        }

        shift %= array.length; // проверить чтоб сдвиг был не больше длинны массива

        if (shift == 0) {
            return;
        }

        int[] temp = new int[shift];
        System.arraycopy(array, array.length - shift, temp, 0, shift);
        System.arraycopy(array, 0, array, shift, array.length - shift);
        System.arraycopy(temp, 0, array, 0, shift);
    }

    public static void main(String[] args) {
        int[] array1 = {1, 3, 2, 7, 4};
        cycleSwap(array1);
        System.out.println(Arrays.toString(array1));

        int[] array2 = {1, 3, 2, 7, 4};
        cycleSwap(array2, 3);
        System.out.println(Arrays.toString(array2));
    }
}
