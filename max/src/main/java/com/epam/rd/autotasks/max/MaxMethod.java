package com.epam.rd.autotasks.max;

public class MaxMethod {
    public static int max(int[] array) {
        int max = array[0]; // Assume the first element as the maximum

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i]; // Update the maximum if a larger element is found
            }
        }

        return max;
    }
}
